FROM anapsix/alpine-java:8_server-jre_unlimited

WORKDIR /app
COPY /target/'api_usuario-0.0.1-SNAPSHOT.jar' .

CMD ["java", "-jar", "api_usuario-0.0.1-SNAPSHOT.jar"]
