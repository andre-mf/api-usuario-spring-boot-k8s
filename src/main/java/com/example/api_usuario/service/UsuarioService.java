package com.example.api_usuario.service;

import com.example.api_usuario.domain.Usuario;
import com.example.api_usuario.dto.UsuarioDTO;
import com.example.api_usuario.exceptions.ObjectNotFoundException;
import com.example.api_usuario.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UsuarioService {

    @Autowired
    private UsuarioRepository usuarioRepository;

    public Usuario save(Usuario usuario) {
        return usuarioRepository.save(usuario);
    }

    public Usuario find(Integer id) {
        Optional<Usuario> obj = usuarioRepository.findById(id);
        return obj.orElseThrow(() -> new ObjectNotFoundException("Objeto não encontrado! Id: " + id + ", Tipo: " + Usuario.class.getName()));
    }

    public List<Usuario> findAll() {
        return usuarioRepository.findAll();
    }

    public Usuario update(Usuario obj) {
        Usuario newObj = find(obj.getId());
        updateData(newObj, obj);
        return usuarioRepository.save(newObj);
    }

    public void delete(Integer id) {
        find(id);
        usuarioRepository.deleteById(id);
    }

    public Usuario fromDto(UsuarioDTO objDto) {
        return new Usuario(objDto.getId(), objDto.getNome(), objDto.getIdade());
    }

    public void updateData(Usuario newObj, Usuario obj) {
        newObj.setNome(obj.getNome());
        newObj.setIdade(obj.getIdade());
    }
}
