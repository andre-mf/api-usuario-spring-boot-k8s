package com.example.api_usuario.config;

import com.example.api_usuario.domain.Usuario;
import com.example.api_usuario.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDate;
import java.util.Arrays;

@Configuration
public class TestConfig implements CommandLineRunner {

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Override
    public void run(String... args) throws Exception {

        usuarioRepository.deleteAll();

        Usuario u1 = new Usuario(null, "João", LocalDate.now().getYear() - 1985);
        Usuario u2 = new Usuario(null, "Maria", LocalDate.now().getYear() - 1957);
        Usuario u3 = new Usuario(null, "José", LocalDate.now().getYear() - 2016);
        Usuario u4 = new Usuario(null, "Francisca", LocalDate.now().getYear() - 1954);

        usuarioRepository.saveAll(Arrays.asList(u1, u2, u3, u4));
    }
}
