package com.example.api_usuario.controller;

import com.example.api_usuario.domain.Usuario;
import com.example.api_usuario.dto.UsuarioDTO;
import com.example.api_usuario.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;

@Controller
@RequestMapping(path = "/usuario")
public class UsuarioController {

    @Autowired
    private UsuarioService usuarioService;

    @PostMapping()
    public ResponseEntity<Void> insert(@RequestBody UsuarioDTO obj) {
        Usuario usuario = usuarioService.fromDto(obj);
        usuario = usuarioService.save(usuario);
        URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri()
                .path("/{id}").buildAndExpand(usuario.getId()).toUri();
        return ResponseEntity.created(uri).build();
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<Usuario> find(@PathVariable Integer id) {
        Usuario obj = usuarioService.find(id);
        return ResponseEntity.ok().body(obj);
    }

    @GetMapping()
    public ResponseEntity<List<Usuario>> findAll() {
        List<Usuario> usuarios = usuarioService.findAll();
        return ResponseEntity.ok().body(usuarios);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<Void> update(@RequestBody UsuarioDTO objDto, @PathVariable Integer id) {
        Usuario obj = usuarioService.fromDto(objDto);
        obj.setId(id);
        obj = usuarioService.update(obj);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Void> delete(@PathVariable Integer id) {
        usuarioService.delete(id);
        return ResponseEntity.noContent().build();
    }
}
